<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<body>
	<h2>Spring MVC and List Example</h2>

	<c:if test="${not empty lists}">
       <table border="1" cellpadding="5">
      
		<tr>
            <td><b>Title</b></td>
            <td><b>Href</b></td>
            <td><b>Ingredients</b></td>
            <td><b>Thumbnail</b></td>
        </tr>
			<c:forEach var="listValue" items="${lists}">
				<tr>
      				<td><c:out value="${listValue.title}" /></td>
      				<td><c:out value="${listValue.href}" /></td>
      				<td><c:forEach items="${listValue.ingredients}" var="item" varStatus="cellStatus">
					<c:out value="${item}"/>
					<c:if test="${!cellStatus.last}">,&nbsp;</c:if>
      				</c:forEach></td>
      				<td><c:out value="${listValue.thumbnail}" /></td>
    			</tr>
			</c:forEach>

 </table>
	</c:if>
</body>
</html>