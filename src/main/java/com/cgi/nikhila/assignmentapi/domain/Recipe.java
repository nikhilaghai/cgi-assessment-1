package com.cgi.nikhila.assignmentapi.domain;

import java.util.List;

public class Recipe {
	private String title;
	private String href;
	private List<String> ingredients;
	private String thumbnail;
	
	//default constructor
	public Recipe() {
		
	}
	
	//parameterised constructor
	public Recipe(String title, String href, List<String> ingredients, String thumbnail) {
		super();
		this.title = title;
		this.href = href;
		this.ingredients = ingredients;
		this.thumbnail = thumbnail;
	}
	//getters and setters
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public List<String> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<String> ingredients) {
		this.ingredients = ingredients;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	

	
}
