package com.cgi.nikhila.assignmentapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.cgi.nikhila.assignmentapi.domain.Recipe;
import com.cgi.nikhila.assignmentapi.service.RecipesService;

@RestController
public class RecipesController {
	@Autowired
	private RecipesService recipesService;

	@RequestMapping(value = "/recipes", method = RequestMethod.GET)
	public ModelAndView getAllRecipes() {
		List<Recipe> list = recipesService.getAllRecipes();
		ModelAndView model = new ModelAndView("displayRecipes");
		model.addObject("lists", list);
		return model;

	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView displayHome() {
		ModelAndView model = new ModelAndView("welcome");
		return model;

	}
	
	@RequestMapping("/userChoiceRecipes")
	public List<Recipe> getRecipesWithUserChoices(@RequestParam String[] ingredients) {
		return recipesService.getRecipesWithUserChoices(ingredients);
	}

}
