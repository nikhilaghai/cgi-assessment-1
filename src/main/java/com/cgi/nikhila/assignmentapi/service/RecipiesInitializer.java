package com.cgi.nikhila.assignmentapi.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.cgi.nikhila.assignmentapi.domain.Recipe;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RecipiesInitializer {
	public static List<Recipe> recipesList;
	@Autowired
	ObjectMapper mapper;
	
	@PostConstruct
    public List<Recipe> getInitialRecipes() {
		try {
			recipesList = (Arrays
					.asList(mapper.readValue(ResourceUtils.getFile("classpath:json/recipes.json"), Recipe[].class)));
			return recipesList;
		} 
		catch (IOException ioException) {
			ioException.printStackTrace();
		} 
		return recipesList;
	}
}
