package com.cgi.nikhila.assignmentapi.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.cgi.nikhila.assignmentapi.domain.Recipe;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public class RecipesService {
	@Autowired
	RecipiesInitializer recipiesInitializer;

	public List<Recipe> getAllRecipes() {
		return recipiesInitializer.getInitialRecipes();
	}

	public List<Recipe> getRecipesWithUserChoices(String[] ingredients) {
		List<Recipe> listToReturn = new ArrayList<Recipe>();
		if(ingredients!=null) {
		List<String> userPreferenceList = Arrays.asList(ingredients);
		
		if (recipiesInitializer.getInitialRecipes() != null) {
			for (Recipe recipe : recipiesInitializer.getInitialRecipes()) {
				if (recipe.getIngredients().containsAll(userPreferenceList)) {
					listToReturn.add(recipe);
				}
			}
			listToReturn.sort((o1, o2) -> o1.getTitle().compareTo(o2.getTitle()));
		}
		}
		return listToReturn;
	}
}
