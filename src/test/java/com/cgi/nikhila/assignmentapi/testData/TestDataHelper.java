package com.cgi.nikhila.assignmentapi.testData;

import java.util.ArrayList;
import java.util.List;

import com.cgi.nikhila.assignmentapi.domain.Recipe;

public class TestDataHelper {

	
	public List<Recipe> getRecipeList(){
		List<Recipe> recipeList=new ArrayList<>();
		Recipe recipe = new Recipe();
		recipe.setTitle("Paneer Tikka");
		recipe.setHref("www.google.com");
		recipe.setThumbnail("www.google.com");
		List<String> ingredients=new ArrayList<String>();
		ingredients.add("Onion");
		ingredients.add("capsicum");
		recipe.setIngredients(ingredients);
		recipeList.add(recipe);
		
		Recipe recipe2 = new Recipe();
		recipe2.setTitle("Pizza");
		recipe2.setHref("www.google.com");
		recipe2.setThumbnail("www.google.com");
		List<String> ingredientsList=new ArrayList<String>();
		ingredients.add("Onion");
		ingredients.add("capsicum");
		ingredients.add("Mushroom");
		recipe2.setIngredients(ingredientsList);
		recipeList.add(recipe2);
		return recipeList;
	}
	
	public String[] getUserIngredients(){
		String[] ingredients= new String[10];
		ingredients[0] = "Onion";
		ingredients[1] = "capsicum";
		return ingredients;
	}
}
