package com.cgi.nikhila.assignmentapi.controller;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.cgi.nikhila.assignmentapi.controller.RecipesController;
import com.cgi.nikhila.assignmentapi.domain.Recipe;
import com.cgi.nikhila.assignmentapi.service.RecipesService;
import com.cgi.nikhila.assignmentapi.testData.TestDataHelper;

@RunWith(MockitoJUnitRunner.class)
public class RecipesControllerTest {
	@InjectMocks
	private RecipesController recipesControllerMock=new RecipesController();
	@Mock
	private RecipesService recipesServiceMock;
	
	private TestDataHelper testDataHelper= new TestDataHelper();
	
	@Test
	public void testGetRecipesWithUserChoices() throws Exception {
		when(recipesServiceMock.getRecipesWithUserChoices(null)).thenReturn(null);
		recipesControllerMock.getRecipesWithUserChoices(null);
		Mockito.verify(recipesServiceMock).getRecipesWithUserChoices(null);

	}
}
