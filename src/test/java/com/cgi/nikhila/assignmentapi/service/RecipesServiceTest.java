package com.cgi.nikhila.assignmentapi.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.cgi.nikhila.assignmentapi.domain.Recipe;
import com.cgi.nikhila.assignmentapi.service.RecipesService;
import com.cgi.nikhila.assignmentapi.service.RecipiesInitializer;
import com.cgi.nikhila.assignmentapi.testData.TestDataHelper;


@RunWith(MockitoJUnitRunner.class)
public class RecipesServiceTest {
	@InjectMocks
	private RecipesService recipeServiceMock=new RecipesService();
	@Mock
	private RecipiesInitializer recipeInitiaserMock;
	
	private TestDataHelper testDataHelper= new TestDataHelper();
	
	
	@Test
	public void testGetAllRecipes() {	
		when(recipeInitiaserMock.getInitialRecipes()).thenReturn(testDataHelper.getRecipeList());
		List<Recipe> recipeList=recipeServiceMock.getAllRecipes();
	    assertTrue(recipeList.size()>0);
		Mockito.verify(recipeInitiaserMock).getInitialRecipes();
	}
	
	@Test
	public void testGetAllRecipesWithEmptyJsonFile()  {	
		when(recipeInitiaserMock.getInitialRecipes()).thenReturn(new ArrayList<Recipe>());
		List<Recipe> recipeList=recipeServiceMock.getAllRecipes();
	    assertEquals(0,recipeList.size());
	    Mockito.verify(recipeInitiaserMock).getInitialRecipes();
	}

	
	@Test
	public void testGetRecipesWithUserChoices() {
		when(recipeInitiaserMock.getInitialRecipes()).thenReturn(testDataHelper.getRecipeList());
		assertNotNull(recipeServiceMock.getRecipesWithUserChoices(testDataHelper.getUserIngredients()));
	}
	
	@Test
	public void testGetRecipesWithNoIngredient() {
		assertEquals(0,recipeServiceMock.getRecipesWithUserChoices(null).size());

	}
	
}
